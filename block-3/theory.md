# Block 3: Typescript

1. Typescript is compiled to JS, so there's no runtime for it at all, Typescript types just don't exist at the moment the code is executed. The only thing you can do is to check the shape of the objects. It is possible to do by using type guards:
```typescript
type MyType = {
  field1: number
  field2: string
}

function isMyType(test: any): test is MyType {
  return ((test as MyType).field1 !== undefined && (test as MyType).field2 !== undefined);
}
```

2. Private methods/variables can be only accessed by the class itself. Protected methods/variables can also be accessed by the descendants of this class.

3. Interfaces are OOP concepts that bring the notion of generalization. They are used for simplification of the development process via abstraction. It will be easier for me to explain if I just give an example of my personal experience with the usage of interfaces. During my studies, I once had to participate in the building of a compiler for some toy language. In the compiler construction, there's a notion of AST (Abstract Syntax Tree), which is basically a tree data structure, which stores all the language entities (declarations, statements, expressions and etc.) You can already see how handy interfaces would be here. We implemented all the top-level entities as abstract classes (e.g. `Expression`) and then expanded them via normal classes, which could already be actual nodes of the AST (e.g. `Addition`, `Multiplication` and etc.) Later, when you perform semantic analysis of the AST, you don't need to know what the current node actually is, it's just enough to know that it's an Expression to perform necessary checks. This simplified the development process and the code structure drastically.

4. There's a notion of Class Constructor, which is basically a function, that is called when you type `new MyClass(//your arguments go here)`.
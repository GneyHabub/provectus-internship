/* Type guards */
class Bird {
  fly() {
    console.log("fly");
  }
}

class Fish {
  swim() {
    console.log("swim");
  }
}

type Pet = Bird | Fish;

function isBird(test: Pet): test is Bird {
  return ((test as Bird).fly !== undefined);
}

function isFish(test: Pet): test is Fish {
  return ((test as Fish).swim !== undefined);
}

function action(pet: Pet): void {
  if (isBird(pet)){
    pet.fly();
  } else if(isFish(pet)) {
    pet.swim();
  }
}

/* Interface */
// Fill in the types for Employee interface
// * `level` field must have one the following values: 'Junior' || 'Middle' || 'Senior';
type Level = 'Junior' | 'Middle' | 'Senior';

interface Employee {
  name: string;
  level: Level;
  age: number;
  skills: string[];
};

// Create a type with fields from above being all read-only
type ReadonlyEmployee = Readonly<{
  name: string;
  level: Level;
  age: number;
  skills: string[];
}>;

// Create a CompanyEmployee class that implements ReadonlyEmployee interface
class CompanyEmployee implements ReadonlyEmployee {
  readonly name: string;
  readonly level: Level;
  readonly age: number;
  readonly skills: string[];

  constructor(name: string, level: Level, age: number, skills: string[]){
    this.name = name;
    this.level = level;
    this.age = age;
    this.skills = skills;
  };
}

// Say, we have an employee object of the `CompanyEmployee` class with a `level` of `Junior`:
// const oldEmployee = new CompanyEmployee(..., 'Junior', ...);
//
// We want to update this employee and make his level "Senior". If we do this
// const newEmployee = new CompanyEmployee(...)
// newEmployee.level = 'Senior' // this doesn't work
//
// How do we get a `newEmployee` object with all fields from `oldEmployee`, but with `.level` set to 'Senior'?
// hint: check how spread operator applies to objects
const oldEmployee = new CompanyEmployee('Putin', 'Junior', 18, ['VueJS', 'Django']);
const newEmployee: CompanyEmployee = {...oldEmployee, level: 'Senior'};

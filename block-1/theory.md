# Block 1: Common

1. Primitives are data that is not an object and has no methods. There are four most common ones - 
_String, Number, Boolean, Undefined_. Also, there are _BigInt and Symbol_. `null` is not a primitive, because it is a special case of `Object`. Primitives are widely used because, in the end, every object consists of primitives.

2. Keyword `this` refers to an object it belongs to. Inside of methods `this` refers to the object that owns this method (or undefined if it is run in the strict mode). Also, `this` can refer to the global object if it is used outside of any user-defined object.

3. A callback is a function that is passed to another function, that needs to be run whenever the latter one finishes executing. It is widely used in asynchronous functions because JS doesn't wait until some function finishes its execution and just proceeds. But what if we need the value that's returned by this async function somewhere later? That's where we use callbacks.

4. The function declaration is a statement which declares a function (thanks, cap). We name it, specify parameters, tell it what to do and what to return. There are several ways to do that. First, the classic one is by using the `function` keyword.
```javascript
function add(a, b) {
  return a + b;
}
```
Another way is to use _arrow functions_:
```javascript
const add = (a, b) => a + b; 
```
One of the differences is that arrow functions don't define their own execution context. Meaning that `this` will refer to the outer method, no matter, where it was called (the same is applied to the `arguments` keyword). In the normal declaration, the behaviour of `this` depends on whether it is a function (defined in the global scope) or a method (defined as a part of an object). As a consequence, arrow functions cannot be used as constructors. 

5. The spread operator does exactly what it is expected to do - spreads values of an iterable (array, for example). Rest operator allows us to pass an indefinite number of arguments to a function. 
```javascript
const spread = ...[1, 2, 3];

const rest = (...args) => {
  return args.map(arg => arg + 1);
}
```
We can say that in some sense they are opposite of each other. Spread turns an array to just a set of values, rest turns function arguments to an array.
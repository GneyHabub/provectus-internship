# PROVECTUS INTERNSHIP TASKS

This repo consists of 3 folders, each one of them contains tasks from the respective block of exercises. 

Each folder has `theory.md` file with answers to the theoretical tasks. Folders 1 and 3 have `index.js` (or `.ts`) files with solutions to the practical tasks. The second block is a bit more complex. If you want to run the program run following commands:
```shell
cd block-2
npm i
npm run start
```
This will run the console app. Whenever you execute some function, it will print the execution time to the console. You can also log the output of the functions.

Or (And) if you want to examine the code, you will find all the interfaces in `src/types.ts` and the program itself in `src/index.ts`. `src/utils.ts` contains some auxillary functions.
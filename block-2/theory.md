# Block 2: Async

1. JS is synchronous. It is one-threaded, meaning only one operation can be performed at a time. It might be sorta "asynchronous" if it makes API calls to some endpoint or the file system and this is when all the callbacks and promises appear.

2. I don't really know what so special about the Call Stack. I guess, it has the same properties as all the stacks, e.g. LIFO execution. If some function calls another function inside of its body, the outer one cannot finish before the inner one finishes. This is how stacks work in general. 

3. AJAX stands for _Asynchronous JavaScript and XML_. It is the set of technologies that allow synchronous JavaScript client to perform asynchronous calls in the background.

4. If the promises are independent, then we can use `Promise.all`. If they are linearly dependent, then we can use power of JS to make our code look prettier. For example, instead of this:
```javascript
function1().then((res1) => function2(res1).then((res2) => function3(res2)));
```
We can write this:
```javascript
function1()
  .then(function2)
  .then(function3)
```
 But in general I'd just use `async/await` and let Babel take care of the backwards compatibility with older browsers.

5. The first one and the most obvious is a try/catch:
```javascript
try {
  // some potentially dangerous action here
} catch(e) {
  console.error(e);
}
```
Another one is to use promise's resolve and reject:
```javascript
const some_promise = new Promise(function(reslove, reject) {
  // asynchronous calls
});

some_promise.then(onResolve, onReject);
```

6. There are lots of different frameworks out there, but as I understand, these questions prompt me to choose between React, Vue and Angular. Maybe Svelte as well. And of course, the choice of the framework depends on the project. For small projects, Svelte is the best, but for large applications better to choose React or Vue because of their scalability. I personally would choose Vue. And here's why:
* I know it well, better than any other framework, simply had more experience with it.
* It is easy. Things like two-way binding with `v-model`, iterating over objects, class and style bindings make my life a bit easier.
* It implies code structure (maybe that's why Vue is a framework and React is still officially called a library). Yeah, one might say that they don't want to depend on technical decisions made by other people and want to make them themselves, and I totally understand this mindset. But most of the time it is not that important, so we can spend our mind-fuel on the app itself and not think about how to structure our components. Also, the implied code structure means that all developers will structure their code in the same way. It simplifies our lives as well.
* Implied tools usage. The same point as above. It was a surprise for me when I first time got to know that React has several state management libraries. And no standard one.
* I just enjoy writing it. I think this is the most important reason.
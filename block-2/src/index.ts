import nodeFetch, {Response} from "node-fetch";
import {prompt} from 'enquirer';
import {Todo, User, Answer} from './types';
import {askNumber, printResult} from './utils';

const url = 'https://jsonplaceholder.typicode.com/todos';
const loadData = async () => await nodeFetch(url).then((response: Response) => response.json());

const data: Promise<Todo[]> = loadData();

// Actual implementations of all the functions
const operations = {
  selectAll: async () => await data,

  selectById: async (id: number): Promise<Todo[]> => {
    const res = await data.then(books => books.filter((book) => book.id === id));
    return res;
  },
  groupByUser: async (): Promise<{[key: number]: Todo[]}> => {
    const todos = await data;
    let res: {[key: number]: Todo[]} = {};
    return todos.reduce((acc, todo): {[key: number]: Todo[]} => {
      acc[todo.userId] ? acc[todo.userId].push(todo) : acc[todo.userId] = [todo];
      return acc;
    }, res);
  },
  selectByUser: async (id: number): Promise<Todo[]> => {
    const res = await data.then(todos => todos.filter((todo) => todo.userId === id));
    return res;
  },
  selectByName: async (name: string): Promise<Todo[]> => {
    const res = await data.then(todos => todos.filter((todo) => todo.title === name));
    return res;
  },
  getAuthor: async (id: number): Promise<User> => {
    const resp = await nodeFetch(`https://jsonplaceholder.typicode.com/users/${id}`);
    const user = await resp.json();
    return user;
  }
};

// Cache variables, used in memorization
let todos: Todo[] = [];
let users: User[] = [];
let groupedByUser: {[key: number]: Todo[]} = {};
let selectedByUser: number[] = []; 

// The wrapper function
const router = async () => {
  const response: { command: keyof typeof operations } = await prompt({
    type: 'autocomplete',
    name: 'command',
    message: 'Command:',
    initial: 0,
    choices: Object.keys(operations),
  });

  switch (response.command) {
    case ('selectAll'): {
      console.time('selectAll');
      let ans: Todo[];

      if (todos.length !== 200) {
        const res = await operations[response.command]();
        todos = res;
        ans = res;
      } else {
        ans = todos;
      }
      console.timeEnd('selectAll');

      await printResult(ans);
      break;
    } 
    case ('selectById'): {
      const todoId = await askNumber('Provide the id of the Todo:');

      console.time('selectById');
      const todo = todos.find(el => el.id === todoId);
      let ans: Todo;
      if (!todo) {
        const res = await operations[response.command](todoId);
        todos.push(res[0]);
        ans = res[0];
      } else {
        ans = todo;
      }
      console.timeEnd('selectById');

      await printResult(ans);
      break;
    } 

    case ('groupByUser'): {
      console.time('groupByUser');
      let ans: {[key: number]: Todo[]};
      if (!Object.keys(groupedByUser).length) {
        const res = await operations[response.command]();
        groupedByUser = res;
        ans = res;
      } else {
        ans = groupedByUser;
      }
      console.timeEnd('groupByUser');

      await printResult(ans);
      break;
    } 

    case ('selectByUser'): {
      const userId = await askNumber('Provide the id of the User:');

      console.time('selectByUser');
      let ans: Todo[];
      if (!selectedByUser.find(el => el === userId)) {
        const res = await operations[response.command](userId);
        todos.push(...res);
        selectedByUser.push(userId)
        ans = res;
      } else {
        ans = todos.filter(el => el.userId === userId);
      }
      console.timeEnd('selectByUser');

      await printResult(ans);
      break;
    } 

    case ('selectByName'): {
      const name: Answer = await prompt({
        type: 'input',
        name: 'name',
        message: 'Provide the name of the todo:',
        validate: (input) => !!input,
      });

      console.time('selectByName');
      const todo = todos.find(el => el.title === name.name);
      let ans: Todo;
      if (!todo) {
        const res = await operations[response.command](name.name);
        todos.push(res[0]);
        ans = res[0];
      } else {
        ans = todo;
      }
      console.timeEnd('selectByName');

      await printResult(ans);
      break;
    } 

    case ('getAuthor'): {
      const todoId = await askNumber('Provide the id of the todo: ');

      console.time('getAuthor');
      const todo = await operations.selectById(todoId);
      const user = users.find(el => el.id === todo[0].userId);
      let ans: User;
      if (!user) {
        const res = await operations[response.command](todo[0].userId);
        users.push(res);
        ans = res;
      } else {
        ans = user;
      }
      console.timeEnd('getAuthor');

      await printResult(ans);
      break;
    } 

    default: {
      console.error('something went wrong...')
    }
  }

  await router();
};

router();
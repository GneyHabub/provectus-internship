import {prompt} from 'enquirer';
import { Answer } from './types';

// Auxillary function for printing the results of an operation 
export const printResult = async (res: object) => {
  const response: Answer = await prompt({
    type: 'confirm',
    name: 'question',
    message: 'Do want to print the result?'
  });
  if(response.question) {
    console.log(res);
  }
}

// Auxillaty function for asking user for a numerical input
export const askNumber = async (question: string): Promise<number> => {
  const res: Answer = await prompt({
    type: 'input',
    name: 'answer',
    message: question,
    validate: (input) => !!input,
  });
  return parseInt(res.answer);
}